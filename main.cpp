#include <iostream> //Wczytanie zależności
#include <cstdlib>
#include <iomanip>
#include <ctime>
#include <climits>

using namespace std; //Używamy przestrzeni nazw std. Nie występują w kodzie żadne kolizje nazw.
	
const int n = 10;

struct pair_number_letter { //Definicja struktury range
	char uppercase_letter;
	int number; 
};

void wypelnij(pair_number_letter X[n][n], int g, int p1, int p2) {
	//Wypełnienie tablicy A[n][n] losowymi liczbami
	for(int i = 0; i < n*n; i++) {
		X[(i/n)][(i%n)].uppercase_letter = (rand()%(p2-p1+1)) + p1;
		X[(i/n)][(i%n)].number = (rand()%(g+1));
	}
}

void drukuj(pair_number_letter X[n][n]) {
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			cout << setw(5) << X[i][j].number << setw(1) << X[i][j].uppercase_letter;
		}
		cout << endl;
	}
}

void zamien(pair_number_letter X[n][n], int number, char character) {
	int sum[n];

	//Wypełnienie sum[n] zerami
	for(int i = 0; i < n; i++) {
		sum[i] = 0;
	}
	//Zliczenie sumy pól liczbowych
	for(int i = 0; i < n; i++) {
		for(int j = 0; j < n; j++) {
			sum[i] += X[i][j].number;
		}
	}

	//Zastąpienie pól znakowych znakiem character jeśli suma pól liczbowych jest podzielna przez number
	for(int i = 0; i < n; i++) {
		if(sum[i]%number == 0) {
			for(int j = 0; j < n; j++) {
				X[i][j].uppercase_letter = character;
			}
		}
	}
}

/*
* Główna funkcja inicjalizująca program
*/

int main() {
	srand(time(NULL)); //wywołanie srand
	pair_number_letter A[n][n];
	int diagonal_max = INT_MIN;
	int index_x = n - 1;
	int index_y = 0;
	int random_y = rand()%n;
	pair_number_letter temp;

	wypelnij(A, 20, 65, 90);
	drukuj(A);

	zamien(A, 2, '@');
	cout << endl << endl;
	drukuj(A);

	//Znalezienie maksymalnej wartości na drugiej przekątnej oraz indeksu tej wartości
	for(int i = 0; i < n; i++) {
		if(A[n-i-1][i].number > diagonal_max) {
			diagonal_max = A[n-i-1][i].number;
			index_x = n-i-1;
			index_y = i;
		}
	}
	cout << endl << endl << "Losujemy wartosc z kolumny: " << random_y + 1 << endl << endl;
	
	temp = A[n-1][random_y];
	A[n-1][random_y] = A[index_x][index_y];
	A[index_x][index_y] = temp;

	cout << endl << endl;

	drukuj(A);
	
	return 0;
}
